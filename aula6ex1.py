### Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:
#
# capacidade total
# capacidade atual
# movimento
# Os comportamentos esperados para um Ônibus são:
# Embarcar
# Desembarcar
# Acelerar
# Frear

#Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
#de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.

class Onibus:

    def __init__(self):
        self.capacidade_total = 45
        self.passageiros = 0
        self.movimento = False

    def embarcar(self, novos_passageiros):
        if self.movimento == True:
            return("O onibus está em movimento!")
        if self.passageiros == self.capacidade_total:
            return("O onibus já está lotado!")
   
        if self.passageiros + novos_passageiros > self.capacidade_total:
            total_passageiros = self.passageiros + novos_passageiros
            passageiros_de_fora = total_passageiros - self.capacidade_total
            conseguiram_entrar = novos_passageiros - passageiros_de_fora
            self.passageiros = 45
            return(f"Apenas {conseguiram_entrar} passageiros conseguiram entrar no onibus, {passageiros_de_fora} ficaram de fora.")

        else:
            self.passageiros += novos_passageiros
            return(f"Todos os {novos_passageiros} passageiros conseguiram entrar.")


    def desembarcar(self, passageiros_saindo):
        if self.movimento == True:
            return("O onibus está em movimento!")
        if self.passageiros == 0:
            return("O onibus já está vazio!")

        if self.passageiros < passageiros_saindo:
            return(f"Há apenas {self.passageiros} pessoas no onibus, não há como {passageiros_saindo} pessoas desembarcarem.")
        else:
            self.passageiros -= passageiros_saindo
            return(f"{passageiros_saindo} passageiros sairam do onibus.")


    def acelerar(self):
        if self.movimento == False:
            self.movimento = True
            return("O onibus começou a se mover.")
        else:
            return("O onibus já está em movimento.")
        
    def frear(self):
        if self.movimento == True:
            self.movimento = False
            return("O onibus parou de se mover.")
        else:
            return("O onibus já está parado.")

onibus_escolar = Onibus()

print(onibus_escolar.acelerar())
print(onibus_escolar.frear())
print(onibus_escolar.embarcar(15))
print(onibus_escolar.acelerar())
print(onibus_escolar.embarcar(5))
print(onibus_escolar.frear())
print(onibus_escolar.embarcar(5))
print(onibus_escolar.embarcar(45))
print(onibus_escolar.acelerar())
print(onibus_escolar.frear())
print(onibus_escolar.desembarcar(20))


    








        
        

   

