import sqlite3
from random import choice

conexao = sqlite3.connect("estoque.db")
cursor = conexao.cursor()

tabela_comida = """
CREATE TABLE comidas (
id integer primary key autoincrement, 
nome, 
preco, 
quantidade)"""

tabela_bebida = """
CREATE TABLE bebidas (
id integer primary key autoincrement, 
nome, 
preco, 
quantidade)
"""

try:
    cursor.execute(tabela_comida)
    cursor.execute(tabela_bebida)

    comidas = [("Hamburger", 25.00, 5), ("Hotdog", 12.00, 5), ("Chocolate", 5.00, 5), ("Pão", 1.50, 5), ("Misto quente", 7.50, 5)]
    bebidas = [("Coca cola", 10.00, 7), ("Fanta", 8.00, 7), ("Café", 2.50, 7)]
    for comida in comidas:
        regristar_comida = f'INSERT INTO comidas (nome, preco, quantidade) VALUES ("{comida[0]}", "{comida[1]}", "{comida[2]}")'
        cursor.execute(regristar_comida)

    for bebida in bebidas:
        regristar_bebida = f'INSERT INTO bebidas (nome, preco, quantidade) VALUES ("{bebida[0]}", "{bebida[1]}", "{bebida[2]}")'
        cursor.execute(regristar_bebida)

    conexao.commit()


except sqlite3.OperationalError:
    pass

def checa_info(tabela, coluna, pedido):
    checa_estoque = f'SELECT {coluna} FROM {tabela} WHERE nome = "{pedido}"'
    cursor.execute(checa_estoque)
    conteudo = cursor.fetchall()

    return conteudo[0][0]

def cliente():
    coleta_comidas = "SELECT nome FROM comidas"
    cursor.execute(coleta_comidas)
    lista_comidas = cursor.fetchall()

    coleta_bebidas = "SELECT nome FROM bebidas"
    cursor.execute(coleta_bebidas)
    lista_bebidas = cursor.fetchall()

    pedido = {}
    pedido["comida"] = choice(lista_comidas)[0]
    pedido["bebida"] = choice(lista_bebidas)[0]
    quantidade_atual_comida = int(checa_info("comidas", "quantidade", pedido["comida"]))
    quantidade_atual_bebida = int(checa_info("bebidas", "quantidade", pedido["bebida"]))

    if quantidade_atual_comida == 0:
        print(f'O item {pedido["comida"]} está fora de estoque.')
  
    elif quantidade_atual_bebida == 0:
        print(f'O item {pedido["bebida"]} está fora de estoque.')

    else:
        atualiza_quantidade_comida = f'UPDATE comidas SET quantidade = {quantidade_atual_comida - 1} WHERE nome = "{pedido["comida"]}"'
        atualiza_quantidade_bebida = f'UPDATE bebidas SET quantidade = {quantidade_atual_bebida - 1} WHERE nome = "{pedido["bebida"]}"'

        cursor.execute(atualiza_quantidade_comida)
        cursor.execute(atualiza_quantidade_bebida)
        conexao.commit()

        preco_comida = float(checa_info("comidas", "preco", pedido["comida"]))
        preco_bebida = float(checa_info("bebidas", "preco", pedido["bebida"]))
        preco_total = preco_comida + preco_bebida

        print(f'Pedido feito! {pedido["comida"]} + {pedido["bebida"]}! Preço: R${preco_total:.2f}')

    for x in range(0, 30):
        cliente()

