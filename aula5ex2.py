### sorteio  
# =============================================================
# 2) Crie um programa que pergunte para o usuario um numero de pessoas a participarem de um sorteio (2-20),
#  e o numero de pessoas a serem sorteadas, e depois sorteie esse numero de pessoas da lista.
# O programa deverá pegar o numero de pessoas a participar aleatoriamente desta lista:
# Nota: A mesma pessoa não pode ganhar duas vezes.
    
from random import randint, choice, shuffle
from time import sleep

lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
"Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
"Benedito", "Tereza", "Valmir", "Joaquim"]






while True:
    numero_part = int(input("Quantas pessoas participarão do sorteio? [02~20]: "))
    if numero_part >= 2 and numero_part <= 20:
        break
    print("Valor inválido.")

n_sorteios = int(input("Quantas pessoas serão sorteadas nesse sorteio? "))

participantes = []
for loop in range(0, numero_part):
    escolhido = choice(lista)
    participantes.append(escolhido)
    lista.remove(escolhido)

print(f"Os participantes serão: {participantes}")
sleep(3)

for sorteio in range(0, n_sorteios):
    ganhador = choice(participantes)
    print(f"{ganhador} venceu o sorteio!")
    participantes.remove(ganhador)
    sleep(1)

