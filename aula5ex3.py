
##=============================================================
# 3) Escreva um programa em python que conte as vogais que a música ‘Faroeste Caboclo’
# tem em sua letra. Armazena a letra da música em um arquivo do tipo txt.
# Dica: Não se esqueça de considerar as letras maiúsculas, minúsculas e com acentuação.
##=============================================================
def checa_vogal(letra):
    if letra.lower() in "aeiouáéíóúãẽĩõũâêîôûàèìòùäëïöü":
        return True
    else:
        return False

vogais = 0
nao_vogais = set()

with open("faroeste.txt", "r", encoding="UTF-8") as arquivo:
    conteudo = arquivo.read()

    for x in conteudo:
        if checa_vogal(x) == True:
            vogais += 1
        else:
            nao_vogais.add(x)

print(f"{vogais} vogais encontradas")
print(f"nao_vogais encontradas: {nao_vogais}")


