
######################################################################################## 
# Implemente um programa que represente uma fila. O contexto do programa é  uma
#  agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra:   
#  o  primeiro
#  a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
#  tir de números os quais representam a idade. A sua fila deverá conter os seguintes
#  comportamentos:
#  .Adicionar pessoa na fila: adicionar uma pessoa na fila.
#  • Atender Fila: atender a pessoa respeitando a ordem de chegada
#  • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila
##########################################################################################
class Fila:

    def __init__(self):
          self._fila = [] 
          self._prior = []         


### checa se o cliente e preferencial ########################################
    def adicionar(self, idade):
        if idade  >= 65: 
            self._prior.append(idade) 
            self._fila.append(idade)  
            return("Cliente prioritario entrou na fila")

        else:    
            self._fila.append(idade)  
            return("Cliente idade {idade} entrou na fila")


### Atendimento################################################################

    def atendimento(self): 
        if len(self._fila) == 0:
            return("Nao existe cliente para ser atendido")
        atendido= self._fila[0]
                        
        if self._fila[0] in self._prior:
               self._prior.remove(atendido) 
               self._fila.remove(atendido)
               
#####prioridade###############################################################
    
    def ver_prioridade(self): 
        if self._prior < 1: 
           return("Nao existe clientes prioritarios para ser atendido")
        atendido= self._fila[0]

    
        priorcli = self._prior[0]
        self._prior.remove(priorcli)
        self._fila.remove(priorcli)
        self._fila.insert(0, priorcli)
        return(f"Pessoa com {priorcli} anos foi priorizada >= 65")            

banco = Fila()

print(banco.adicionar(56))
print(banco.adicionar(66))
print(banco.adicionar(25))

print(banco.atendimento())
print(banco.ver_prioridade())
print(banco.atendimento())


 

