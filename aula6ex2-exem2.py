class Fila:

    def __init__(self):
        self.fila = []        # FILA
        self.prioridade = []  # REGISTRO

    def adicionar(self, idade):
        if idade >= 65:
            self.prioridade.append(idade)
            self.fila.append(idade)
        else:
            self.fila.append(idade)

        return(f"Uma pessoa de {idade} anos entrou na fila.")

    def atender(self):
        if len(self.fila) == 0:
            return("Não há ninguém na fila.")
        
        atendido = self.fila[0]
        if atendido in self.prioridade:
            self.prioridade.remove(atendido)

        self.fila.remove(atendido)
        return(f"Uma pessoa com {atendido} anos foi atendida.")

        
    def dar_prioridade(self):
        if len(self.prioridade) == 0:
            return("Não há ninguém acima de 65 anos na fila.")
        
        prioritario = self.prioridade[0]
        self.prioridade.remove(prioritario)
        self.fila.remove(prioritario)
        self.fila.insert(0, prioritario)

        return(f"Uma pessoa com {prioritario} anos recebeu prioridade e passou na frente.")


banco = Fila()

print(banco.adicionar(17))
print(banco.adicionar(25))
print(banco.adicionar(82))
print(banco.adicionar(56))
print(banco.adicionar(71))
print(banco.adicionar(36))

print(banco.atender())
print(banco.dar_prioridade())
print(banco.atender())

    
