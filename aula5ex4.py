# ====================================================================================
# 4) Escreva um programa em python que realize um cadastro. Deverão ser coletadas as
# seguintes informações:
# CPF
# Nome
# Idade
# Sexo
# Endereço
# Os registros deverão ser armazenados em um arquivo CSV. Caso desejar manter o padrão
# brasileiro, o CSV será separado pelo caractere ;

# # Persistencia de dados com OPEN

# # Modos:
# # w - write  - escrever
# # r - read   - ler
# # a - append - adicionar

#cpf = input("Qual o numm CPF? ")
#nome = input("Qual o seu nome? ")
#idade = input("Qual a sua idade? ")
#sexo = input("Qual o seu sexo M=Masculino F=Feminino? ")
#end= input("Qual o seu endereco? ")


#arquivo = open("cadastro.txt", "a")
#arquivo.write(f"{cpf,nome,idade,sexo,end},delimiter";"\n")
#arquivo.close()

#arquivo = open("cadastro.txt", "r")
#conteudo = arquivo.read()
#print(conteudo)
#arquivo.close()

#with open("cadastro.txt", "a") as arquivo:


import csv

while True:
    opção = input("Gostaria de cadastrar uma nova entrada no banco? [S/N]: ")

    if opção.upper() == "N":
        print("Parando o programa...")
        break

    elif opção.upper() == "S":
        cpf = input("digite o CPF: ")
        nome = input("digite o nome: ")
        idade = input("digite a idade: ")
        sexo = input("digite o sexo: ")
        endereco = input("digite o endereço: ")

        pessoa = (cpf, nome, idade, sexo, endereco)

        with open("banco.csv", "a") as arquivo:
            conteudo = csv.writer(arquivo, delimiter=";")
            conteudo.writerow(pessoa)

    else:
        print("Opção inválida")



