# ====================================================
# Exercicio 4:
# Assumindo que uma lata de tinta pinta 3m², escreva um programa
# que possua uma função que receba as dimenções de uma parede,
# passadas pelo usuario, calcule sua área, e mostre uma mensagem
# dizendo quantas latas de tinta seriam necessárias para pintar
# essa parede.





#var1 = int( input("Digite o comprimento da parede: ") )
#var2 = int( input("Digite a largura da parede: ") )
#calculo = var1 * var2
#print("A metragem da parede é",calculo)
#lata= (calculo / 3)  
#print("Qtde de latas necessarias para pintar a parede= ", lata)

from math import ceil

def area(x, y):
    area_total = x * y
    latas = area_total / 3

    print(f"Para pintar essa parede, voce vai precisar comprar {ceil(latas)} latas de tinta.")

largura = float(input("Qual a largura da parede que deseja pintar? "))
altura = float(input("Qual a altura da parede que deseja pintar? "))

area(largura, altura)
    